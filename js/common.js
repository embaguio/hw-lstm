const getBlueChips = () => {
  return PSE_BLUE_CHIPS;
};

const runSimulation = (algo) => {
  vm.placeholderSimulation = 'Running Simulation';  
  const { companyId: cmpy_id, securityID: security_id } = vm.selectedCompany;
  const startDate = getFormattedDate(new Date(vm.startDate));
  const endDate = getFormattedDate(new Date(vm.endDate));
  const data = { cmpy_id, security_id, startDate, endDate, algo };
  return  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: `${vm.baseUrl}/run-scraper`,
    data: JSON.stringify(data),
    success: result => {
      const res = result && JSON.parse(result);
      if (!res.success || !res.data) {
        vm.placeholderSimulation = "An error occurred.";
        return;
      }
      const keyName = `${algo}Data`;
      vm[keyName] = res;
      vm[keyName] = { ...vm[keyName], imgPath: `${vm.baseUrl}/images/${vm[keyName].graphImg}`};
    }
  });
}


const triggerSimulator = () => {
  vm.hwData = null;
  vm.lstmData = null;
  vm.hwlstmData = null;
  vm.isSimulationRunning = true;
  $.when(
      runSimulation('hw'),
      runSimulation('lstm'),
      runSimulation('hwlstm'),
  ).then(() => {
      vm.isSimulationRunning = false;
      vm.placeholderSimulation = 'Run Simulation';
  });
};

const vm = new Vue({
  el: "#app_hw_lstm",
  data: {
    baseUrl: `${API}/api`,
    companies: getBlueChips(),
    selectedCompany: BDO,
    startDate: START_DATE,
    endDate: END_DATE,
    isSimulationRunning: false,
    placeholderSimulation: "Run Simulation",
    hwData: null,
    lstmData: null,
    hwlstmData: null,
  }
});
