const START_DATE = "2013-01-01";
const END_DATE = "2018-12-31";

const BDO = {
  securityID: "468",
  securitySymbol: "BDO",
  securityName: "BDO UNIBANK, INC.",
  companyId: "260"
};

const PSE_BLUE_CHIPS = [
  {
    securityID: "180",
    securitySymbol: "AC",
    securityName: "AYALA CORPORATION",
    companyId: "57"
  },
  {
    securityID: "183",
    securitySymbol: "AEV",
    securityName: "ABOITIZ EQUITY VENTURES, INC.",
    companyId: "16"
  },
  {
    securityID: "179",
    securitySymbol: "AGI",
    securityName: "ALLIANCE GLOBAL GROUP, INC.",
    companyId: "212"
  },
  {
    securityID: "532",
    securitySymbol: "AP",
    securityName: "ABOITIZ POWER CORP.",
    companyId: "609"
  },
  BDO,
  {
    securityID: "233",
    securitySymbol: "BLOOM",
    securityName: "BLOOMBERRY RESORTS CORPORATION",
    companyId: "49"
  },
  {
    securityID: "101",
    securitySymbol: "BPI",
    securityName: "BANK OF THE PHILIPPINE ISLANDS",
    companyId: "234"
  },
  {
    securityID: "521",
    securitySymbol: "FGEN",
    securityName: "FIRST GEN CORPORATION",
    companyId: "600"
  },
  {
    securityID: "127",
    securitySymbol: "GLO",
    securityName: "GLOBE TELECOM, INC.",
    companyId: "69"
  },
  {
    securityID: "572",
    securitySymbol: "GTCAP",
    securityName: "GT CAPITAL HOLDINGS, INC.",
    companyId: "633"
  },
  {
    securityID: "142",
    securitySymbol: "ICT",
    securityName: "INTERNATIONAL CONTAINER TERMINAL SERVICES, INC.",
    companyId: "83"
  },
  {
    securityID: "207",
    securitySymbol: "JGS",
    securityName: "JG SUMMIT HOLDINGS, INC.",
    companyId: "210"
  },
  {
    securityID: "225",
    securitySymbol: "LTG",
    securityName: "LT GROUP, INC.",
    companyId: "12"
  },
  {
    securityID: "108",
    securitySymbol: "MBT",
    securityName: "METROPOLITAN BANK & TRUST COMPANY",
    companyId: "128"
  },
  {
    securityID: "215",
    securitySymbol: "MEG",
    securityName: "MEGAWORLD CORPORATION",
    companyId: "127"
  },
  {
    securityID: "137",
    securitySymbol: "MER",
    securityName: "MANILA ELECTRIC COMPANY",
    companyId: "118"
  },
  {
    securityID: "526",
    securitySymbol: "MPI",
    securityName: "METRO PACIFIC INVESTMENTS CORPORATION",
    companyId: "604"
  },
  {
    securityID: "567",
    securitySymbol: "PGOLD",
    securityName: "PUREGOLD PRICE CLUB, INC.",
    companyId: "629"
  },
  {
    securityID: "312",
    securitySymbol: "RLC",
    securityName: "ROBINSONS LAND CORPORATION",
    companyId: "195"
  },
  {
    securityID: "589",
    securitySymbol: "RRHI",
    securityName: "ROBINSONS RETAIL HOLDINGS, INC.",
    companyId: "646"
  },
  {
    securityID: "396",
    securitySymbol: "SCC",
    securityName: "Semirara Mining and Power Corporation",
    companyId: "157"
  },
  {
    securityID: "114",
    securitySymbol: "SECB",
    securityName: "SECURITY BANK CORPORATION",
    companyId: "32"
  },
  {
    securityID: "520",
    securitySymbol: "SM",
    securityName: "SM INVESTMENTS CORPORATION",
    companyId: "599"
  },
  {
    securityID: "165",
    securitySymbol: "SMC",
    securityName: "SAN MIGUEL CORPORATION",
    companyId: "154"
  },
  {
    securityID: "314",
    securitySymbol: "SMPH",
    securityName: "SM PRIME HOLDINGS, INC.",
    companyId: "112"
  },
  {
    securityID: "134",
    securitySymbol: "TEL",
    securityName: "PLDT Inc.",
    companyId: "6"
  },
  {
    securityID: "167",
    securitySymbol: "URC",
    securityName: "UNIVERSAL ROBINA CORPORATION",
    companyId: "124"
  }
];
