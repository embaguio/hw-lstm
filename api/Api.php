<?php 
class Api {

	public function runScraper($data) {
		$return = array();
		$return['success'] = false;

		$url = 'https://edge.pse.com.ph/common/DisclosureCht.ax';
		$obj = json_decode($data);
		$datasetPath = 'dataset';

		if (!file_exists($datasetPath)) {
			mkdir($datasetPath, 0777, true);
		}

		$fileName = "{$obj->cmpy_id}_{$obj->security_id}_{$obj->startDate}_{$obj->endDate}.json";
		$file = "{$datasetPath}/{$fileName}";
		$fileExists = is_file($file);

		if($fileExists){
			$contents = file_get_contents($file);
		} else {
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$results = curl_exec($ch);
			$contents = Api::getCleanedData($results);
			curl_close($ch); 

			file_put_contents($file, $contents);
		}
		set_time_limit(900);
		$path = str_replace("\\", "/", realpath(getcwd()));
		$str = "{$path}/model/{$obj->algo}.py {$fileName}";

		$command = shell_exec(escapeshellcmd($str));
		$imgGraphFile = str_replace(".json",".png",$fileName);
		$return['success'] = true;
		$return['graphImg'] = "{$obj->algo}_{$imgGraphFile}";


		$return['data'] = $command;

		echo json_encode($return);
	}

	public function getNormalizeValue($val, $min, $max) {
		$delta = $max - $min;
		$minRange = 1;
		$maxRange = 5;

		return $minRange + (($val - $min)*($maxRange - $minRange) / $delta);
	}

	public function getNormalizedData($data) {
		$result = array();
		$min = $data[0]->CLOSE;
		$max = $data[0]->CLOSE;

		foreach($data as $row){
			$close_price = $row->CLOSE;
			$min = $min < $close_price ? $min : $close_price;
			$max = $max > $close_price ? $max : $close_price;
		}

		foreach($data as $row){
			array_push($result,
			array("date"=>$row->CHART_DATE,
				"closing_price_raw"=>($row->CLOSE),
				"closing_price_normalized"=>Api::getNormalizeValue($row->CLOSE, $min, $max))
			);
		}

		return $result;
	}

	public function getCleanedData($data) {
		$obj = json_decode($data);
		$cleanData = Api::getNormalizedData($obj->chartData);

		return json_encode($cleanData);
	}
}



