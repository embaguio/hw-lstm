<?php
	header('Access-Control-Allow-Origin: *'); 
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
 	error_reporting(E_ALL);
 	ini_set("display_errors", 1);

	$raw = $_SERVER["REQUEST_URI"];

	if($raw[strlen($raw)-1] == "/") {
		$raw = rtrim($raw,"/");
	}

	$params = explode("/",$raw);
	require_once("Routes.php");
	
	if(!isset($params[3])){
		Routes::handleError();
	}
	$route = array("module"=>$params[2], "action"=>$params[3]);
	Routes::executeRoute($route);
