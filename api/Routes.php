<?php
class Routes {
	public static function executeRoute($route) {
		$tmpRoute = str_replace( "-", "", $route["module"] ); 
		$modulePath = ucwords($tmpRoute).".php";
		$module = strtolower($route["module"]);
		$action = strtolower($route["action"]);
		
		if(!file_exists($modulePath)){
			Routes::handleError();
		}

		require_once($modulePath);
		Routes::executeAction($module, $action);
	}

	protected static function executeAction($module, $action) {
		$endpoint = "/".$module."/".$action;

		if($module == "api"){
			$action = new Api();
		}
		
		switch ($endpoint) {	
			case '/api/run-scraper':
				$json = file_get_contents('php://input');
				$action->runScraper($json);
			break;		

			default:
				Routes::handleError();
				break;
		}
	}

	public static function handleError() {
		http_response_code(404);
		$response = array();
		$response["error"] = "Resource Not Found";
		die(json_encode($response));
	}
}